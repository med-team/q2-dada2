Source: q2-dada2
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-setuptools,
               qiime (>= @DEB_VERSION_UPSTREAM@),
               r-bioc-dada2
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/q2-dada2
Vcs-Git: https://salsa.debian.org/med-team/q2-dada2.git
Homepage: https://qiime2.org/
Rules-Requires-Root: no

Package: q2-dada2
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends},
         r-base-core,
         r-bioc-dada2 (>= 1.24.0+dfsg-2~),
         r-cran-optparse,
         qiime (>= @DEB_VERSION_UPSTREAM@),
         q2-types (>= @DEB_VERSION_UPSTREAM@),
         python3-biom-format,
         python3-skbio
Description: QIIME 2 plugin to work with adapters in sequence data
 QIIME 2 is a powerful, extensible, and decentralized microbiome analysis
 package with a focus on data and analysis transparency. QIIME 2 enables
 researchers to start an analysis with raw DNA sequence data and finish with
 publication-quality figures and statistical results.
 Key features:
  * Integrated and automatic tracking of data provenance
  * Semantic type system
  * Plugin system for extending microbiome analysis functionality
  * Support for multiple types of user interfaces (e.g. API, command line,
 graphical)
 .
 QIIME 2 is a complete redesign and rewrite of the QIIME 1 microbiome analysis
 pipeline. QIIME 2 will address many of the limitations of QIIME 1, while
 retaining the features that makes QIIME 1 a powerful and widely-used analysis
 pipeline.
 .
 QIIME 2 currently supports an initial end-to-end microbiome analysis pipeline.
 New functionality will regularly become available through QIIME 2 plugins. You
 can view a list of plugins that are currently available on the QIIME 2 plugin
 availability page. The future plugins page lists plugins that are being
 developed.
 .
 This package wraps the dada2 R package in BioConductor for modeling and
 correcting Illumina-sequenced amplicon errors. This was shown to improve the
 sensitivity of diversity analyses.
